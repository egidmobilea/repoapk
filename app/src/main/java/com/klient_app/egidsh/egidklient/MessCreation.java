package com.klient_app.egidsh.egidklient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

class MessCreation {
    @SuppressLint("SetTextI18n")
    MessCreation(final Activity ac){
        ac.runOnUiThread(() -> {
            if(nope.wypierdolsie) {
                try {
                    LinearLayout ll = ac.findViewById(R.id.llrm);
                    ImageView ivv = ac.findViewById(R.id.imageView3);
                    ivv.clearAnimation();
                    ArrayList<Integer> al = new ArrayList<>();
                    ArrayList<Integer> al2 = new ArrayList<>();
                    ArrayList<View> altmp = new ArrayList<>();
                    for (int i = 0; i < ll.getChildCount(); i++) {
                        altmp.add(ll.getChildAt(i));
                    }
                    for (View Vv : altmp) {
                        ll.removeView(Vv);
                    }
                    for (JSONObject jp : nope.hymm2) {
                        if (jp.getInt("przeczytana") == 0 && jp.getInt("id_nadawca") != nope.idk) {
                            al2.add(jp.getInt("id_nadawca"));
                        }
                        if (jp.getInt("id_odbiorca") != nope.idk && !al.contains(jp.getInt("id_odbiorca"))) {
                            al.add(jp.getInt("id_odbiorca"));
                        } else if (jp.getInt("id_nadawca") != nope.idk && !al.contains(jp.getInt("id_nadawca"))) {
                            al.add(jp.getInt("id_nadawca"));
                        }
                    }
                    for (int ii : al) {
                        RelativeLayout rl = new RelativeLayout(ac);
                        TextView tv = new TextView(ac);
                        ImageView iv = new ImageView(ac);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(nope.convertDpToPixel(250), LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.bottomMargin = nope.convertDpToPixel(5);
                        params.topMargin = nope.convertDpToPixel(5);
                        rl.setOnClickListener(G -> nope.AC(G, ac));
                        rl.setLayoutParams(params);
                        rl.setId(ii);
                        for (JSONObject jow : nope.wew) {
                            if (jow.getInt("id") == ii) {
                                switch (jow.getString("type")) {

                                    case "AND":
                                        tv.setText(jow.getString("login") + " : " + "Android");
                                        break;
                                    case "WEB":
                                        tv.setText(jow.getString("login") + " : " + "Web");
                                        break;
                                    case "PC":
                                        tv.setText(jow.getString("login") + " : " + "Desktop");
                                        break;
                                    case "menager":
                                        tv.setText(jow.getString("login") + " : " + "Menadżer");
                                        break;
                                    case "grafik":
                                        tv.setText(jow.getString("login") + " : " + "Grafik");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        tv.setGravity(Gravity.CENTER_VERTICAL);
                        tv.setTextColor(ac.getResources().getColor(R.color.test));
                        tv.setTextSize(18);
                        tv.setTypeface(Typeface.DEFAULT_BOLD);
                        tv.setId(ii + al.size());
                        par.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                        tv.setLayoutParams(par);
                        iv.setId(ii + (2 * al.size()));
                        rl.addView(tv, par);
                        par = new RelativeLayout.LayoutParams(nope.convertDpToPixel(33), nope.convertDpToPixel(33));
                        if (!al2.contains(ii)) {
                            iv.setVisibility(View.INVISIBLE);
                        }else {
                            iv.startAnimation(AnimationUtils.loadAnimation(ac,R.anim.rott2));
                            ivv.startAnimation(AnimationUtils.loadAnimation(ac,R.anim.rott));
                        }
                        iv.setImageResource(R.mipmap.kopertka23);
                        par.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                        iv.setLayoutParams(par);
                        rl.setBackground(ac.getResources().getDrawable(R.drawable.testujemy));
                        rl.addView(iv, par);
                        ll.addView(rl, params);
                    }
                } catch (Exception ex) {
                    Log.e("object", "Exception5-2: " + ex.getMessage());
                }
            }
        });
    }

}
