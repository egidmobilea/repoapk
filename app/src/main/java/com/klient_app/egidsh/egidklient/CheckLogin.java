package com.klient_app.egidsh.egidklient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.net.URL;
import java.net.URLEncoder;
import java.net.URLConnection;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import android.util.Log;
import org.json.JSONObject;
import java.util.ArrayList;

    public class CheckLogin extends AsyncTask<String, String, String> {
        private boolean wo;
        public interface AsyncResponce {
            void taskFinish(JSONObject[] result);
        }
        private AsyncResponce delegate = null;

        @SuppressLint("StaticFieldLeak")
        private ProgressBar progressBar;
        @SuppressLint("StaticFieldLeak")
        private Context act;
        private ArrayList<String> sl = new ArrayList<>();
        private ArrayList<String> s2 = new ArrayList<>();

        CheckLogin(boolean wys_odb, Context xtc, ProgressBar actt, int qr, String v1, String v2, String v3, String v4, String v5, AsyncResponce delegate){
            wo=wys_odb;
            this.delegate = delegate;
            try {
                if(!v1.equals("NULLS")){
                    sl.add("3vgtq");
                    s2.add(v1);
                }
                if(!v2.equals("NULLS")){
                    sl.add("wacef");
                    s2.add(v2);
                }
                if(!v3.equals("NULLS")){
                    sl.add("v72");
                    s2.add(v3);
                }
                if(!v4.equals("NULLS")){
                    sl.add("w983f765j");
                    s2.add(v4);
                }
                if(!v5.equals("NULLS")){
                    sl.add("cal_days_in_monthn5v");
                    s2.add(v5);
                }
                sl.add("queryvg54");
                s2.add(Integer.valueOf(qr).toString());
                act = xtc;
                progressBar = actt;
            }catch (Exception ex){
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
            }
        }
        private Boolean isSuccess = false;

        @Override
        protected void onPreExecute() {
            if(progressBar!=null) {
                try {
                    if(progressBar.getVisibility()!=View.VISIBLE) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                } catch (Exception ex) {
                    Log.e("object", "pre exe: " + ex.getMessage());
                }
            }
        }

        @Override
        protected void onPostExecute(String r) {
            progressBar.setVisibility(View.INVISIBLE);
            if (!isSuccess) {
                Toast.makeText(act, "Błąd połaczenia z bazą lub brak danych do odebrania", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String link = "https://egidstudio.com/phpdroid.php";
                URL url = new URL(link);
                StringBuilder sbb = new StringBuilder();
                for(int i =0;i<s2.size();i++) {
                    if(i!=0){
                        sbb.append("&");
                    }
                    sbb.append(URLEncoder.encode(sl.get(i), "UTF-8")).append("=").append(URLEncoder.encode(s2.get(i), "UTF-8"));
                }
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                try {
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    Log.e("TakNie",sbb.toString());
                    wr.write(sbb.toString());
                    wr.flush();
                    wr.close();
                }catch (Exception ex){
                    Log.e("errr",ex.getMessage());
                }
                if(wo) {
                    BufferedReader reader = new BufferedReader(new
                            InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                        break;
                    }
                    Log.e("Tag1",sb.toString());
                    ArrayList<JSONObject> jList = new ArrayList<>();
                    String[] splitted = (sb.toString().substring(0, sb.toString().length() - 1)).split("\\}");
                    if (!splitted[0].equals("[]")) {
                        for (int f = 0; f < (splitted.length - 1); f++) {
                            jList.add(new JSONObject((splitted[f].substring(1)) + "}"));
                        }
                        JSONObject[] jObject = jList.toArray(new JSONObject[0]);
                        isSuccess = true;
                        delegate.taskFinish(jObject);
                    }
                    return sb.toString();
                }else if(!wo){
                    try {
                        BufferedReader reader = new BufferedReader(new
                                InputStreamReader(conn.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line).append("\n");
                            break;
                        }
                        Log.e("Tag1",sb.toString());
                    }catch (Exception e){
                        Log.e("object", "NAUCZ SIE NAZYWAC ERROR MESSAGE: " + e.getMessage());
                        if(e.getMessage().equals("https://egidstudio.com/phpdroid.php")){
                            isSuccess = true;
                            JSONObject[] jj;
                            jj = new JSONObject[1];
                            jj[0]= new JSONObject("{\"tak\":\"tak\"}");
                            delegate.taskFinish(jj);
                            return "tak";
                        }
                    }
                }
                return "juz";
            }catch (Exception e){
                Log.e("object", "TY LENIWA KLUCHO: " + e.getMessage());
                return "Exception2: " + e.getMessage();
            }
        }
    }