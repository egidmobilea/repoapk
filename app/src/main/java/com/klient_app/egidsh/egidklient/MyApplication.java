package com.klient_app.egidsh.egidklient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.app.Application;


public class MyApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}