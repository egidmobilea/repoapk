package com.klient_app.egidsh.egidklient;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.app.Notification.Builder;
import android.util.Log;
import android.app.PendingIntent;
import android.app.NotificationManager;
import org.json.JSONObject;
import android.widget.ProgressBar;
import java.util.Timer;
import java.util.TimerTask;
import android.app.NotificationChannel;
import android.graphics.Color;
public class backgroundservices extends Service {
    Timer myTimer;
    public ProgressBar pb;
    boolean is3g, isWifi;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pb = new ProgressBar(MyApplication.getAppContext());
        myTimer = new Timer();
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if(manager!=null) {
            is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting();
            isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting();
        }
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(is3g||isWifi) {
                    CheckLogin checkLogin = new CheckLogin(true, MyApplication.getAppContext(), pb, 2354,Integer.valueOf(nope.idk).toString(),"NULLS","NULLS","NULLS","NULLS", result1 -> {

                        CheckLogin checkLogin1 = new CheckLogin(true, MyApplication.getAppContext(), pb, 864,Integer.valueOf(nope.idk).toString(),"NULLS","NULLS","NULLS","NULLS", result2 -> {

                            if (nope.hymm != result1 || nope.hymm2 != result2) {
                                if (nope.notificationMAAL && nope.notificationPW) {
                                    showNotification(result1, result2);
                                }
                                nope.hymm = result1;
                                nope.hymm2 = new JSONObject[result2.length];
                                for (int i = 0; i < result2.length; i++) {
                                    nope.hymm2[i] = result2[result2.length - 1 - i];
                                }
                            }
                        });
                        checkLogin1.execute();
                    });
                    checkLogin.execute();
                    dataserv.senddata(nope.hymm2, nope.hymm);
                }
            }
        }, 300000, 300000);
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private void showNotification(JSONObject[] jo1, JSONObject[] jo2) {
        Intent intent = new Intent(backgroundservices.this, mainactAL.class);
        Intent intent2 = new Intent(backgroundservices.this, ProWin.class);
        try {
                for (int i = 0; i < nope.hymm.length; i++) {
                    if (!nope.hymm[i].getString("state").equals(jo1[i].getString("state"))){
                        String tmpsx;
                        switch (jo1[i].getInt("state")){
                            case 0:
                                tmpsx="Tworzenie wstępnej specyfikacji projektu";
                                break;
                            case 1:
                                tmpsx="Tworzenie pełnej specyfikacji i szablonu designu";
                                break;
                            case 2:
                                tmpsx="Wstępna implementacja";
                                break;
                            case 3:
                                tmpsx="Pełna implementacja";
                                break;
                            case 4:
                                tmpsx="Pełna implementacja i wstępne testy";
                                break;
                            case 5:
                                tmpsx="Testy końcowe i nanoszenie poprawek";
                                break;
                            case 6:
                                tmpsx="Projekt zakończony";
                                break;
                            case 7:
                                tmpsx="Wsparcie aktywne/pasywne";
                                break;
                            default:
                                tmpsx="";
                                break;
                        }
                        intent2.putExtra("dane",nope.hymm[i].toString());
                        PendingIntent pIntent2 =  PendingIntent.getActivity(backgroundservices.this, (int) System.currentTimeMillis(), intent2, 0);
                        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP  ) {
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            Builder builder = new Notification.Builder(this);
                            builder.setContentTitle("Zmiana statusu projektu")
                                    .setSmallIcon(R.mipmap.e203_k)
                                    .setAutoCancel(true)
                                    .setContentIntent(pIntent2)
                                    .setPriority(Notification.PRIORITY_DEFAULT)
                                    .addAction(R.mipmap.str, "Pokaż", pIntent2);
                            Notification notification = new Notification.BigTextStyle(builder)
                                    .bigText("Twój projekt, " + nope.hymm[i].getString("name") + ", zmienił swój status na " + tmpsx + ".").build();
                            if(notificationManager!=null) {
                                notificationManager.notify(0, notification);
                            }
                        }else if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.N){
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(this)
                                            .setSmallIcon(R.mipmap.e203_k)
                                            .setContentTitle("Zmiana statusu projektu")
                                            .setContentIntent(pIntent2)
                                            .setAutoCancel(true)
                                            .addAction(R.mipmap.str, "Pokaż", pIntent2)
                                            .setStyle(new NotificationCompat.BigTextStyle()
                                                    .bigText("Twój projekt, " + nope.hymm[i].getString("name") + ", zmienił swój status na " + tmpsx + "."));
                            NotificationManager mNotifyMgr =
                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            if(mNotifyMgr!=null) {
                                mNotifyMgr.notify(1, mBuilder.build());
                            }
                        }
                        else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                            Intent intent3 = new Intent("com.klient_app.egidsh.egidklient.ACTION_PLAY");
                            intent3.putExtra("dane", nope.hymm[i].toString());
                            intent3.setClass(this, ProWin.class);
                            PendingIntent pIntent3 =  PendingIntent.getActivity(backgroundservices.this, (int) System.currentTimeMillis(), intent3, 0);
                            NotificationManager mNotifyMgr =
                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            NotificationChannel notificationChannel = new NotificationChannel("com.klient_app.egidsh.egidklient",
                                    "ch1", NotificationManager.IMPORTANCE_LOW);
                            notificationChannel.enableLights(true);
                            notificationChannel.setShowBadge(true);
                            notificationChannel.enableVibration(true);
                            notificationChannel.setSound(null, null);
                            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                            if(mNotifyMgr!=null) {
                                mNotifyMgr.createNotificationChannel(notificationChannel);
                            }
                            Builder builder = new Notification.Builder(getApplicationContext(), "com.klient_app.egidsh.egidklient")
                                    .setSmallIcon(R.mipmap.e203_k)
                                    .setContentTitle("Zmiana statusu projektu")
                                    .setContentIntent(pIntent3)
                                    .setAutoCancel(true)
                                    .setStyle(new Notification.BigTextStyle()
                                    .bigText("Twój projekt, " + nope.hymm[i].getString("name") + ", zmienił swój status na " + tmpsx + "."));
                            if(mNotifyMgr!=null) {
                                mNotifyMgr.notify(1, builder.build());
                            }
                        }
                    }
                }
            }catch(Exception ex){
                Log.e("porównywanie", ex.getMessage());
            }
            if(jo2.length!=nope.hymm2.length) {
                try {
                    StringBuilder tmps3= new StringBuilder();
                    for(int i=nope.hymm2.length;i<jo2.length;i++){
                        tmps3.append(nope.wew[jo2[i].getInt("id_nadawca")].getString("login"));
                        if(i+1<jo2.length){
                            tmps3.append(",");
                        }
                    }
                    tmps3 = new StringBuilder("Otrzymałeś nowe wiadomości od: " + tmps3 + ".");
                    PendingIntent pIntent = PendingIntent.getActivity(backgroundservices.this, (int) System.currentTimeMillis(), intent, 0);

                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP  ) {
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        Builder builder = new Notification.Builder(this);
                        builder.setContentTitle("Nowa wiadomość")
                                .setSmallIcon(R.mipmap.e203_k)
                                .setAutoCancel(true)
                                .setContentIntent(pIntent)
                                .setPriority(Notification.PRIORITY_HIGH)
                                .addAction(R.mipmap.str, "Pokaż", pIntent);
                        Notification notification = new Notification.BigTextStyle(builder)
                                .bigText(tmps3.toString()).build();
                        if(notificationManager!=null) {
                            notificationManager.notify(1, notification);
                        }
                    }else if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.N){
                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(this)
                                        .setSmallIcon(R.mipmap.e203_k)
                                        .setContentTitle("Nowa wiadomość")
                                        .setContentIntent(pIntent)
                                        .setAutoCancel(true)
                                        .addAction(R.mipmap.str, "Pokaż", pIntent)
                                        .setStyle(new NotificationCompat.BigTextStyle()
                                            .bigText(tmps3.toString()));
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        if(mNotifyMgr!=null) {
                            mNotifyMgr.notify(1, mBuilder.build());
                        }
                    }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        NotificationChannel notificationChannel = new NotificationChannel("com.klient_app.egidsh.egidklient",
                                "ch1", NotificationManager.IMPORTANCE_DEFAULT);
                        notificationChannel.enableLights(true);
                        notificationChannel.setLightColor(Color.RED);
                        notificationChannel.setShowBadge(true);
                        notificationChannel.enableVibration(true);
                        notificationChannel.setSound(null, null);
                        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        if(mNotifyMgr!=null) {
                            mNotifyMgr.createNotificationChannel(notificationChannel);
                        }
                        Builder builder = new Notification.Builder(getApplicationContext(), "com.klient_app.egidsh.egidklient")
                                .setSmallIcon(R.mipmap.e203_k)
                                .setContentTitle("Nowa wiadomość")
                                .setContentIntent(pIntent)
                                .setAutoCancel(true)
                                .setStyle(new Notification.BigTextStyle()
                                        .bigText(tmps3.toString()));

                        if(mNotifyMgr!=null) {
                            mNotifyMgr.notify(1, builder.build());
                        }
                    }
                } catch (Exception ex) {
                    Log.e("porównywanie", ex.getMessage());
                }
            }

        }

}


