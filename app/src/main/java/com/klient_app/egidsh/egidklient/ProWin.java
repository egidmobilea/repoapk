package com.klient_app.egidsh.egidklient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ProWin extends AppCompatActivity implements View.OnTouchListener{
    JSONObject JOpr;
    TextView Pn;
    ImageView iv;
    ViewGroup _root;
    EditText edti;
    EditText edmes;
    ImageButton ehx;
    public ProgressBar pb2,pb3;
    private int _xDelta,_yDelta,_xDelta2,_yDelta2,exlvGP = -1;
    boolean wys = false,wys2 = false;
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    private GestureDetector gd;
    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        Intent intent = getIntent();
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pro_win);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            ((TextView)findViewById(R.id.NRb)).setText("Numer buildu: "+pInfo.versionName);
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            long time = ze.getTime();
            ((TextView)findViewById(R.id.BDate)).setText("Data buildu: "+SimpleDateFormat.getInstance().format(new java.util.Date(time)));
        } catch (PackageManager.NameNotFoundException ignore) {
        } catch (IOException ignore) {
        }
        edti = findViewById(R.id.editText4);
        edmes = findViewById(R.id.editText3);
        edti.setText(nope.tem);
        edmes.setText(nope.tes);
        ehx = findViewById(R.id.imageButton2);
        gd = (new GestureDetector(this, new gesty(this)));
        pb2 = findViewById(R.id.progressBar2);
        pb3 = findViewById(R.id.progressBar3);
        Pn = findViewById(R.id.textView3);
        iv = findViewById(R.id.imageView);
        RelativeLayout LLM = findViewById(R.id.llm);
        nope.odbwysmin = LLM.getLayoutParams().height;
        nope.odbwysmax = nope.convertDpToPixel(900);
        MessCreation tmpMC =  new MessCreation(this);
        TextView odbiorca = findViewById(R.id.textView8);
        odbiorca.setText("Odbiorca: Menadżer");
        RelativeLayout LLNM = findViewById(R.id.llnm);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) LLNM.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) LLM.getLayoutParams();
        _xDelta = layoutParams.bottomMargin;
        _xDelta2 = layoutParams2.rightMargin;
        _root = findViewById(R.id.RL);
        LLNM.setOnTouchListener(this);
        LLM.setOnTouchListener(this);
        try {
            String tmpst = intent.getStringExtra("dane");
            JOpr = new JSONObject(tmpst);
            Pn = findViewById(R.id.textView3);
            Pn.setText(JOpr.getString("name"));
            Pn = null;
            Pn = findViewById(R.id.textView11);
            TextView tv4 = findViewById(R.id.textView4);
            switch (JOpr.getInt("state")){
                case 0:
                    tv4.setText("Tworzenie wstępnej specyfikacji projektu");
                    pb3.setProgress(0);
                    break;
                case 1:
                    tv4.setText("Tworzenie pełnej specyfikacji i szablonu designu");
                    pb3.setProgress(10);
                    break;
                case 2:
                    tv4.setText("Wstępna implementacja");
                    pb3.setProgress(25);
                    break;
                case 3:
                    tv4.setText("Pełna implementacja");
                    pb3.setProgress(45);
                    break;
                case 4:
                    tv4.setText("Pełna implementacja i wstępne testy");
                    pb3.setProgress(65);
                    break;
                case 5:
                    tv4.setText("Testy końcowe i nanoszenie poprawek");
                    pb3.setProgress(80);
                    break;
                case 6:
                    tv4.setText("Projekt zakończony");
                    pb3.setProgress(100);
                    break;
                case 7:
                    tv4.setText("Wsparcie aktywne/pasywne");
                    pb3.setProgress(100);
                    break;
                default:
                    break;
            }
            String tmpS = JOpr.getString("archiv");
            tmpS = tmpS.replace("\\n","\n");
            Pn.setText(tmpS);
            Pn = findViewById(R.id.textView5);
            tmpS = JOpr.getString("opis");
            Pn.setText(tmpS);
            iv = findViewById(R.id.imageView);
            switch (JOpr.getString("p_type")) {
                case "AND":
                    iv.setImageResource(R.mipmap.phone);
                    break;
                case "PC":
                    iv.setImageResource(R.mipmap.net);
                    break;
                case "WEB":
                    iv.setImageResource(R.mipmap.ziemia);
                    break;
                default:
                    break;
            }

        }catch(Exception ex) {
            Log.e("butt", ex.getMessage());
        }
        expandableListView = findViewById(R.id.expandableListView);
        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(groupPosition-> exlvGP=groupPosition);
        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id)-> {
            odbiorca.setText(expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition));
            String tmpss[] = expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition).split(":");
            String yee="dawsd";
            Log.e("htege",tmpss[0]);
            try {
                for (JSONObject joj : nope.wew) {
                    if (joj.getString("login").equals(tmpss[0].substring(0,tmpss[0].length()-1))) {
                        nope.ids = joj.getInt("id");
                    }
                }
            }catch (Exception ignored){

            }
            expandableListView.collapseGroup(groupPosition);
            return false;
        });
        dataserv.setdanezuslugi((V,z)->{
                    nope.hymm2 = V;
                    nope.hymm = z;
                    MessCreation tmpMC2 = new MessCreation(this);
                }
        );
        if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
            ehx.setOnClickListener(V->wyslij(V));
            ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
        }else {
            ehx.setOnClickListener(V->{});
            ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
        }
        edti.addTextChangedListener(textWatcher);
        edmes.addTextChangedListener(textWatcher);
    }
    public TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
            nope.tem = edti.getText().toString();
            nope.tes = edmes.getText().toString();
            mainactAL.edmes.setText(edmes.getText().toString());
            mainactAL.edti.setText(edti.getText().toString());
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
        }
    };
    @Override
    public boolean onTouchEvent(MotionEvent event){
        return gd.onTouchEvent(event);
    }
    @Override
    public void onPause(){
        super.onPause();
        nope.notificationPW =true;
    }
    @Override
    public void onResume(){
        super.onResume();
        nope.notificationPW=false;
    }
    long time2= 0,time= 0;
    int deltatime2 = 0,delatmargin2 = 0,deltatime = 0,delatmargin = 0;
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(final View view, MotionEvent event) {
        if(view == findViewById(R.id.llnm)) {
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    view.clearAnimation();
                    _yDelta = Y;
                    time = System.currentTimeMillis();
                    break;
                case MotionEvent.ACTION_UP:
                    RelativeLayout.LayoutParams layoutParamsx = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    if (layoutParamsx.bottomMargin >= _xDelta - (_xDelta / 4) && !wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp - ((tmp + _xDelta / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.bottomMargin > _xDelta / 20) {
                                    params.bottomMargin = _xDelta / 20;
                                    wys = true;
                                }
                            }
                        };
                        if (delatmargin == 0) {
                            a.setDuration(500);
                        } else {
                            if (((tmp + _xDelta / 20) / delatmargin) * deltatime < 1500 && ((tmp + _xDelta / 20) / delatmargin) * deltatime >= 1) {
                                a.setDuration(((tmp + _xDelta / 20) / delatmargin) * deltatime);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        view.startAnimation(a);
                        wys = true;
                    } else if (layoutParamsx.bottomMargin <= _xDelta / 4 && wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp + ((_xDelta - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        if (delatmargin == 0) {
                            a.setDuration(500);
                        } else {
                            if (((_xDelta - tmp) / delatmargin) * deltatime * -1 < 1500 && ((_xDelta - tmp) / delatmargin) * deltatime * -1 >= 1) {
                                a.setDuration(((_xDelta - tmp) / delatmargin) * deltatime * -1);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        if (exlvGP != -1) {
                            expandableListView.collapseGroup(exlvGP);
                        }
                        view.startAnimation(a);
                        wys = false;
                    } else if (layoutParamsx.bottomMargin < _xDelta - (_xDelta / 4) && !wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp + ((_xDelta - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    } else if (layoutParamsx.bottomMargin > _xDelta / 4 && wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp - ((tmp + _xDelta / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.bottomMargin > _xDelta / 20) {
                                    params.bottomMargin = _xDelta / 20;
                                }
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    _yDelta = Y - _yDelta;
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.bottomMargin -= _yDelta;
                    if (layoutParams.bottomMargin < _xDelta) {
                        layoutParams.bottomMargin = _xDelta;
                        wys = false;
                    } else if (layoutParams.bottomMargin > _xDelta / 20) {
                        layoutParams.bottomMargin = _xDelta / 20;
                        wys = true;
                    }
                    view.setLayoutParams(layoutParams);
                    delatmargin = _yDelta;
                    _yDelta = Y;
                    deltatime = (int) (System.currentTimeMillis() - time);
                    time = System.currentTimeMillis();
                    break;
            }
        }else if(view == findViewById(R.id.llm)&&nope.trigerek){
            final int Y = (int) event.getRawX();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    view.clearAnimation();
                    _yDelta2 = Y;
                    time2 = System.currentTimeMillis();
                    break;
                case MotionEvent.ACTION_UP:
                    RelativeLayout.LayoutParams layoutParamsx = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    if (layoutParamsx.rightMargin >= _xDelta2 - (_xDelta2 / 4) && !wys2) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.rightMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.rightMargin = (int) (tmp - ((tmp + _xDelta2 / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.rightMargin > _xDelta2 / 20) {
                                    params.rightMargin = _xDelta2 / 20;
                                    wys2 = true;
                                }
                            }
                        };
                        if (delatmargin2 == 0) {
                            a.setDuration(500);
                        } else {
                            if (((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2 < 1500 && ((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2 >= 1) {
                                a.setDuration(((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        view.startAnimation(a);
                        wys2 = true;
                    } else if (layoutParamsx.rightMargin <= _xDelta2 / 4 && wys2) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.rightMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.rightMargin = (int) (tmp + ((_xDelta2 - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        if (delatmargin2 == 0) {
                            a.setDuration(500);
                        } else {
                            if (((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1 < 1500 && ((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1 >= 1) {
                                a.setDuration(((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        view.startAnimation(a);
                        wys2 = false;
                    } else if (layoutParamsx.rightMargin < _xDelta2 - (_xDelta2 / 4) && !wys2) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.rightMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.rightMargin = (int) (tmp + ((_xDelta2 - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    } else if (layoutParamsx.rightMargin > _xDelta2 / 4 && wys2) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.rightMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.rightMargin = (int) (tmp - ((tmp + _xDelta2 / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.rightMargin > _xDelta2 / 20) {
                                    params.rightMargin = _xDelta2 / 20;
                                }
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    _yDelta2 = Y - _yDelta2;
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.rightMargin -= _yDelta2;
                    if (layoutParams.rightMargin < _xDelta2) {
                        layoutParams.rightMargin = _xDelta2;
                        wys2 = false;
                    } else if (layoutParams.rightMargin > _xDelta2 / 20) {
                        layoutParams.rightMargin = _xDelta2 / 20;
                        wys2 = true;
                    }
                    view.setLayoutParams(layoutParams);
                    delatmargin2 = _yDelta2;
                    _yDelta2 = Y;
                    deltatime2 = (int) (System.currentTimeMillis() - time2);
                    time2 = System.currentTimeMillis();
                    break;
            }
        }
        _root.invalidate();
        return true;
    }
    public void specs(View v){
        if(findViewById(R.id.spec).getVisibility() == View.VISIBLE){
            findViewById(R.id.spec).setVisibility(View.GONE);
        }else {
            findViewById(R.id.spec).setVisibility(View.VISIBLE);
        }
    }
    public void puff(View v){
        v.setVisibility(View.GONE);
    }
    public void wyslij (View v) {
        if (wys) {
            EditText edti = findViewById(R.id.editText4);
            EditText edmes = findViewById(R.id.editText3);
            if (!edti.getText().toString().equals("") && !edmes.getText().toString().equals("")) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                String strDate = format.format(now);
                CheckLogin checkLogin = new CheckLogin(false, ProWin.this, findViewById(R.id.progressBar2), 6564,  Integer.valueOf(nope.idk).toString() ,  Integer.valueOf(nope.ids).toString() , edmes.getText().toString(), edti.getText().toString(),strDate, resultp ->nope.refresh(pb2));
                checkLogin.execute("");
                edti.setText("");
                nope.tem = "";
                edmes.setText("");
                nope.tes = "";
                RelativeLayout lltmp = findViewById(R.id.llnm);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lltmp.getLayoutParams();
                final int tmp = params.bottomMargin;
                Animation a = new Animation() {

                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lltmp.getLayoutParams();
                        params.bottomMargin = (int)(tmp + ((_xDelta-tmp) * interpolatedTime));
                        lltmp.setLayoutParams(params);
                    }
                };
                a.setDuration(250);
                lltmp.startAnimation(a);
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
