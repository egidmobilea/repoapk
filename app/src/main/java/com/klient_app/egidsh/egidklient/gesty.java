package com.klient_app.egidsh.egidklient;

import android.app.Activity;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class gesty implements GestureDetector.OnGestureListener {
    private Activity acc;
    gesty(Activity ac){
        acc =ac;
    }
    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if(e2.getX() - e1.getX() > 150){
            acc.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            acc.finish();
            acc.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
        }
        return false;
    }
}
