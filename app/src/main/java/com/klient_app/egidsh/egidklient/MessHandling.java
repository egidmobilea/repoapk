package com.klient_app.egidsh.egidklient;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONObject;
import java.util.ArrayList;

class MessHandling {
    private static int convertDpToPixel(float dp){
        Resources resources = MyApplication.getAppContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int)px;
    }
    MessHandling(final Activity ac, final View v, final boolean wys){

        ac.runOnUiThread(() -> {
            try {
                TextView tv1, tv2, tv3;
                LinearLayout ll2 = ac.findViewById(R.id.llrm);
                LinearLayout ll = ac.findViewById(R.id.llrm2);
                for (int i = 0; i < ll2.getChildCount(); i++) {
                    View V1 = ll2.getChildAt(i);
                    if (V1.getId() != v.getId()) {
                        if (wys) {
                            V1.setVisibility(View.GONE);
                        } else {
                            V1.setVisibility(View.VISIBLE);
                        }
                    }
                }
                ArrayList<View> altmp = new ArrayList<>();
                for (int i = 0; i < ll.getChildCount(); i++) {
                    View V1 = ll.getChildAt(i);
                    altmp.add(V1);
                }
                for (View Vv : altmp) {
                    ll.removeView(Vv);
                }
                if (wys) {
                    for (JSONObject jp : nope.hymm2) {
                        int x = jp.getInt("id");
                        if (x > 0) {
                            if (jp.getInt("id_nadawca") == v.getId() || jp.getInt("id_odbiorca") == v.getId()) {
                                tv1 = new TextView(ac);
                                tv2 = new TextView(ac);
                                tv3 = new TextView(ac);
                                tv1.setTextSize(25);
                                tv2.setTextSize(13);
                                tv3.setTextSize(20);
                                tv1.setId(x + (2 * nope.hymm2.length));
                                tv2.setId(x + (3 * nope.hymm2.length));
                                tv3.setId(x + (4 * nope.hymm2.length));
                                LinearLayout.LayoutParams llpmx = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                try {
                                    if (jp.getInt("id_nadawca") == v.getId()) {
                                        tv1.setGravity(3);
                                        tv2.setGravity(3);
                                        llpmx.setMargins( convertDpToPixel(4), 5,  convertDpToPixel(10), 30);
                                    } else {
                                        tv1.setGravity(5);
                                        tv2.setGravity(5);
                                        llpmx.setMargins( convertDpToPixel(20), 5,  convertDpToPixel(10), 30);
                                    }
                                } catch (Exception ex) {
                                    Log.e("to nie winno wyskoczyc", ex.getMessage());
                                }
                                tv3.setGravity(Gravity.START);
                                tv1.setTypeface(Typeface.DEFAULT, 1);
                                LinearLayout.LayoutParams llpm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                llpm.setMargins( convertDpToPixel(2), 20, convertDpToPixel(5), 10);
                                ll.addView(tv1, llpm);
                                llpm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                llpm.setMargins( convertDpToPixel(2), 5, convertDpToPixel(5), 5);
                                ll.addView(tv2, llpm);
                                ll.addView(tv3, llpmx);
                                tv1.setTextColor(ac.getResources().getColor(R.color.test3));
                                tv3.setTextColor(ac.getResources().getColor(R.color.test3));
                                tv1.setText(jp.getString("tytul"));
                                tv2.setText(jp.getString("date"));
                                tv3.setText(jp.getString("text"));

                            }
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e("ogolnie cos nie dziala", ex.getMessage());
            }
        });
    }


}
