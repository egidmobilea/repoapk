package com.klient_app.egidsh.egidklient;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.net.Uri;
import android.content.DialogInterface;
import android.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Base64;
import org.json.JSONObject;
import android.os.Message;
import android.content.SharedPreferences;
import android.view.WindowManager;
import android.view.Window;
import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile; //... Serio? ... Muszę?  ... Eh ... TO SĄ BIBLIOTEKI!!!!!!!!!!!!

public class MainActivity extends AppCompatActivity { //Witamy w startowej klasie inicjującej layout logowania oraz obsługującej system logowania, itp. Wywołania: ... domyślne?
    Button login; //Przycisk logowania
    ProgressBar progressBar; //progress bar logowania (chyba można wywalić jak brak BCyrpta
    public EditText username, password; //pola tekstowe z hasłem i loginem
    public ProgressBar pb; //eeeeeee dummy progressbar?
    int PMid; //id z pref'ów

    boolean is3g, isWifi; //sprawdzanie czy jest połączenia z internetem?
    @Override
    protected void onCreate(Bundle savedInstanceState) { //Startowa funckaja tworząca layout logowania, WYwołania: domyślne Wywołuje do: CheckLogin (83,86,87,122(resvoid)) Zmienne do nope'a: id, projekty, wiadomości, pracownicy projektów
        MainActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        try { //mój system generowania daty buildu :>
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            ((TextView)findViewById(R.id.NRb)).setText("Numer buildu: "+pInfo.versionName);
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            long time = ze.getTime();
            ((TextView)findViewById(R.id.BDate)).setText("Data buildu: "+ SimpleDateFormat.getInstance().format(new java.util.Date(time)));
        } catch (PackageManager.NameNotFoundException | IOException ignore) {
        }
        login = findViewById(R.id.button);
        progressBar = getWindow().getDecorView().findViewById(R.id.progressBar); //Dziwne patrz niżej
        username = findViewById(R.id.editText);
        password = findViewById(R.id.editText2);
        pb = MainActivity.this.getWindow().getDecorView().findViewById(R.id.progressBar); //Dziwne patrz wyżej
        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", MODE_PRIVATE); //Prefy
        PMid = pref.getInt("id", -1); //Prefy
        String PMpAss = pref.getString("pAss", "000000000000000000000000000000000000000vcs="); //Prefy
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE); //Sprawdzamy czy jest net?
        if(manager!=null) {
            is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting();
            isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting();
        } //Kończymy sprawdzać?

        if (PMid >= 0) { //Jak są prefy to autologowanie
            if (is3g || isWifi) {
                    nope.idk = PMid; //id z prefów do nopa
                    final Intent intent = new Intent(MainActivity.this, mainactAL.class); //nowa aktywność przygotowania
                    Intent ikurwo = new Intent(MyApplication.getAppContext(), backgroundservices.class); //up
                    CheckLogin checkLogin = new CheckLogin(true, MainActivity.this, pb,696969,Integer.valueOf(PMid).toString(), PMpAss,"NULLS","NULLS","NULLS" , resultCL -> { //projekty i weryfikacja pref'ów
                        nope.hymm = resultCL;
                        ProgressBar dummy1 = new ProgressBar(MyApplication.getAppContext());
                        CheckLogin checkLogin2 = new CheckLogin(true, MainActivity.this, pb,864,Integer.valueOf(PMid).toString(),"NULLS","NULLS","NULLS","NULLS", res2 -> { //Wiadomości?
                            CheckLogin checkLogin3 = new CheckLogin(true, MainActivity.this, dummy1, 627,"NULLS","NULLS","NULLS","NULLS","NULLS", res3 -> { //to jest puste O.o (brak zmiennych przekazanych) ... pracownicy?

                                nope.hymm2 = new JSONObject[res2.length]; //Witaj JSON
                                for (int i = 0; i < res2.length; i++) {
                                    nope.hymm2[i] = res2[res2.length - 1 - i];
                                }//Żegnaj
                                nope.wew = res3; //a więc pracownicy
                                nope.idk = PMid; // ... ale to było ... (79)
                                MyApplication.getAppContext().startService(ikurwo);
                                MainActivity.this.startActivity(intent);//odpalamy nowe okno :D
                            });
                            checkLogin3.execute("");
                        });
                        checkLogin2.execute("");
                    });
                    checkLogin.execute("");
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);//animki przejść
            } else {
                Toast.makeText(MyApplication.getAppContext(), "Włącz połączenie internetowe, aby się zalogowac.", Toast.LENGTH_SHORT).show();//jak nie ma neta
            }
        }
        login.setOnClickListener(v -> {
                EditText username = MainActivity.this.findViewById(R.id.editText);
                EditText password = MainActivity.this.findViewById(R.id.editText2);
                final String usernam = username.getText().toString();
                final String passwordd = password.getText().toString();
                if (usernam.equals("") || passwordd.equals("")) { //sprawdza czy są wpisane login i hasło
                    Toast.makeText(MainActivity.this, "Oba pola muszą być wypełnione!", Toast.LENGTH_LONG).show();
                } else {
                    if(isWifi||is3g) {
                        if(findViewById(R.id.spec2).getVisibility() == View.VISIBLE){ //chyba to powiadomienie o czekaniu (wywalić)
                            findViewById(R.id.spec2).setVisibility(View.GONE);
                        }else {
                            findViewById(R.id.spec2).setVisibility(View.VISIBLE);
                        }
                            CheckLogin checkLogin = new CheckLogin(true, this, pb, 36836,usernam,"NULLS","NULLS","NULLS","NULLS", this::resvoid); //zaczynamy logowanie, pobiera dane użytkowanika z loginem
                            checkLogin.execute();
                    }else{
                        Toast.makeText(MyApplication.getAppContext(), "Włącz połączenie internetowe, aby się zalogować.", Toast.LENGTH_SHORT).show();
                    }
                }
        });

    }
    public void newclient1(View view){ //otwiera naszą stronę po kliknięciu w logo
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.egidstudio.com"));
        startActivity(browserIntent);
    }

    void resvoid (final JSONObject[] resx){
        final String passwordd = password.getText().toString();
        boolean usreq = false;
        boolean passgood = false;
        Message msg = new Message();
        try {
            if (resx[0].getString("type").equals("user")) { //sprawdzamy tym logowanego konta
                usreq = true;
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(passwordd.getBytes(StandardCharsets.UTF_8));
                if (resx[0].getString("password").equals(Base64.encodeToString(hash, Base64.DEFAULT).substring(0,Base64.encodeToString(hash, Base64.DEFAULT).length()-1))) { //porównanie haseł i zamiana sha256 z lokalnego na base64
                    passgood = true;
                    final Intent intent = new Intent(MainActivity.this, mainactAL.class);
                    Intent ikurwo= new Intent(MyApplication.getAppContext(), backgroundservices.class);
                    final int id = resx[0].getInt("id");
                    nope.idk=id;
                    CheckLogin checkLogin = new CheckLogin(true,MainActivity.this, pb, 2354,Integer.valueOf(id).toString(),"NULLS","NULLS","NULLS","NULLS", res -> {
                        nope.hymm = res;
                        try {
                            SharedPreferences pref = MainActivity.this.getApplicationContext().getSharedPreferences("pref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("pAss", resx[0].getString("password"));
                            editor.putInt("id",id);
                            editor.apply();
                        } catch (Exception ex) {
                            Log.e("object", "Exception2: " + ex.getMessage());
                        }
                        CheckLogin checkLogin2 = new CheckLogin(true, MainActivity.this, pb, 864,Integer.valueOf(id).toString(),"NULLS","NULLS","NULLS","NULLS", res2 -> {
                            CheckLogin checkLogin3 = new CheckLogin(true, MainActivity.this, pb, 627,"NULLS","NULLS","NULLS","NULLS","NULLS", res3 -> {
                                try {
                                    nope.hymm2 = new JSONObject[res2.length];
                                    for (int i = 0; i < res2.length; i++) {
                                        nope.hymm2[i] = res2[res2.length - 1 - i];
                                    }
                                    nope.wew = res3;
                                    MyApplication.getAppContext().startService(ikurwo);
                                    MainActivity.this.startActivity(intent);
                                } catch (Exception ex) {
                                    Log.e("object1", "Exception1: " + ex.getMessage() + " " + resx[0]);
                                }
                            });
                            checkLogin3.execute("");
                        });
                        checkLogin2.execute("");

                    });
                        checkLogin.execute("");
                        MainActivity.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        } catch (Exception ex) {
            Log.e("object", "Exception1: " + ex.getMessage()+" "+resx[0]);
        }
        if (!usreq) {
            Looper.prepare();
            Toast.makeText(MyApplication.getAppContext(), "Zaloguj się z aplikacji wewnętrznej." , Toast.LENGTH_SHORT).show();
            Looper.myLooper().quit();
        }
        if (!passgood) {
            Looper.prepare();
            Toast.makeText(MyApplication.getAppContext(), "Hasło nieprawidłowe." , Toast.LENGTH_SHORT).show();
            Looper.myLooper().quit();
        }
    }
    public void specs(View v){
        if(findViewById(R.id.spec).getVisibility() == View.VISIBLE){
            findViewById(R.id.spec).setVisibility(View.GONE);
        }else {
            findViewById(R.id.spec).setVisibility(View.VISIBLE);
        }
    }
    public void puff(View v){
        v.setVisibility(View.GONE);
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Czy chcesz wyjść z aplikacji?").setPositiveButton("Tak", dialogClickListener)
                .setNegativeButton("Nie", dialogClickListener).show();
    }
    DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
    };
}
