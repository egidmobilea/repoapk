package com.klient_app.egidsh.egidklient;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.json.JSONObject;
import android.widget.LinearLayout.LayoutParams;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.view.animation.Transformation;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.view.WindowManager;
import android.view.Window;
import android.graphics.Typeface;

public class mainactAL extends AppCompatActivity implements View.OnTouchListener {
    public LinearLayout cl,lld;
    public TextView tv,tv2;
    public ImageView iv;
    ViewGroup _root;
    private int _xDelta,_yDelta,_xDelta2,_yDelta2,exlvGP = -1;
    boolean wys = false,wys2 = false;
    public ProgressBar pb2;
    @SuppressLint("StaticFieldLeak")
    static EditText edti;
    @SuppressLint("StaticFieldLeak")
    static EditText edmes;
    ImageButton ehx;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mainact_al);
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            ((TextView)findViewById(R.id.NRb)).setText("Numer buildu: "+pInfo.versionName);
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
            ZipFile zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            long time = ze.getTime();
            ((TextView)findViewById(R.id.BDate)).setText("Data buildu: "+SimpleDateFormat.getInstance().format(new java.util.Date(time)));
        } catch (PackageManager.NameNotFoundException ignore) {
        } catch (IOException ignore) {
        }
        edti = findViewById(R.id.editText4);
        edmes = findViewById(R.id.editText3);
        ehx = findViewById(R.id.imageButton2);
        RelativeLayout LLM = findViewById(R.id.llm);
        pb2 = mainactAL.this.getWindow().getDecorView().findViewById(R.id.progressBar2);
        nope.odbwysmin = LLM.getLayoutParams().height;
        nope.odbwysmax = nope.convertDpToPixel(900);
        MessCreation tmpMC =  new MessCreation(this);
        TextView odbiorca = findViewById(R.id.textView8);
        odbiorca.setText("Odbiorca: Menadżer");
        RelativeLayout LLNM = findViewById(R.id.llnm);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) LLNM.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) LLM.getLayoutParams();
        _xDelta = layoutParams.bottomMargin;
        _xDelta2 = layoutParams2.rightMargin;
        _root = findViewById(R.id.RL);
        LLNM.setOnTouchListener(this);
        LLM.setOnTouchListener(this);
        dataserv.setdanezuslugi((V,z)->{
                    nope.hymm2 = V;
                    nope.hymm = z;
                    MessCreation tmpMC2 = new MessCreation(this);
                }
        );

            int x = -1;
            LinearLayout ll = mainactAL.this.getWindow().getDecorView().findViewById(R.id.llz);
            for(JSONObject jp : nope.hymm){
                try {
                    x = jp.getInt("id");
                }catch (Exception ex){
                    Log.e("object", "Exception3: " + ex.getMessage());
                }
                if(x>=0) {
                    cl = new LinearLayout(this);
                    LayoutParams lp = new LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0,10,0,10);
                    cl.setLayoutParams(lp);
                    cl.setBackground(getResources().getDrawable(R.drawable.borders));
                    tv = new TextView(this);
                    tv.setTextSize(18);
                    tv.setTypeface(Typeface.DEFAULT_BOLD);
                    iv = new ImageView(this);
                    tv2 = new TextView(this);
                    tv2.setTextSize(15);
                    lld = new LinearLayout(this);
                    try{
                        switch (jp.getString("p_type")) {
                            case "AND":
                                tv.setText("Aplikacja Mobilna");
                                iv.setImageResource(R.mipmap.phone);
                                break;
                            case "PC":
                                tv.setText("Aplikacja Desktopowa");
                                iv.setImageResource(R.mipmap.net);
                                break;
                            case "WEB":
                                tv.setText("Projekt WEB'owy");
                                iv.setImageResource(R.mipmap.ziemia);
                                break;
                            default:
                                break;
                        }
                        tv2.setText(jp.getString("name"));
                    }catch (Exception ex){
                        Log.e("object", "Exception3: " + ex.getMessage());
                    }
                    cl.setId(x);
                    LinearLayout.LayoutParams cllp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,nope.convertDpToPixel(40));
                    cllp.setMargins(20,20,0,20);
                    iv.setAdjustViewBounds(true);
                    iv.setLayoutParams(cllp);
                    cl.addView(iv,cllp);
                    ll.addView(cl);
                    cllp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                    cllp.setMargins(25,20,5,20);
                    lld.setLayoutParams(cllp);
                    lld.setOrientation(LinearLayout.VERTICAL);
                    cl.addView(lld,cllp);
                    LinearLayout.LayoutParams cllpx = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                    tv.setLayoutParams(cllpx);
                    lld.addView(tv,cllpx);
                    cllpx = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                    tv2.setLayoutParams(cllpx);
                    lld.addView(tv2,cllpx);
                    Animation animShow = AnimationUtils.loadAnimation(this, R.anim.testr);
                    animShow.setStartOffset(150*x);
                    cl.startAnimation(animShow);
                    cl.setOnClickListener(v-> {
                            for(JSONObject jotmp:nope.hymm) {
                                try {
                                    if(jotmp.getInt("id") == v.getId()) {
                                        newAc(jotmp);
                                        break;
                                    }
                                }catch (Exception ex){
                                    Log.e("object", "Exception3: " + ex.getMessage());
                                }
                            }
                    });
                    x=-1;
                }
            }

        expandableListView = findViewById(R.id.expandableListView);
        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(groupPosition-> exlvGP=groupPosition);
        expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id)-> {
                       odbiorca.setText(expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition));
                       String tmpss[] = expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition).split(":");
                       Log.e("htege",tmpss[0]);
                       try {
                           for (JSONObject joj : nope.wew) {
                               if (joj.getString("login").equals(tmpss[0].substring(0,tmpss[0].length()-1))) {
                                   nope.ids = joj.getInt("id");
                               }
                           }
                       }catch (Exception ignored){

                       }
                       expandableListView.collapseGroup(groupPosition);
                return false;
        });
        edti.addTextChangedListener(textWatcher);
        edmes.addTextChangedListener(textWatcher);
    }
    public TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(V->wyslij(V));
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
            nope.tem = edti.getText().toString();
            nope.tes = edmes.getText().toString();
        }
    };
    @Override
    public void onPause(){
        super.onPause();
        nope.notificationMAAL =true;
    }
    @Override
    public void onResume(){
        super.onResume();
        nope.notificationMAAL=false;
        if (edmes != null && edti != null){
            mainactAL.edmes.setText(nope.tes);
            mainactAL.edti.setText(nope.tem);
            if(!edti.getText().toString().equals("")&&!edmes.getText().toString().equals("")){
                ehx.setOnClickListener(this::wyslij);
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2));
            }else {
                ehx.setOnClickListener(V->{});
                ehx.setBackground(getResources().getDrawable(R.drawable.bordersdark2off));
            }
        }
    }
    public void newAc(JSONObject JOp){
        try {
            Intent intent = new Intent(mainactAL.this, ProWin.class);
            intent.putExtra("dane", JOp.toString());
            startActivity(intent);
            this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }catch(Exception ex){
            Log.e("thefuck",ex.toString());
        }
    }
    long time2= 0,time= 0;
    int deltatime2 = 0,delatmargin2 = 0,deltatime = 0,delatmargin = 0;
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(final View view, MotionEvent event) {
        if(view == findViewById(R.id.llnm)) {
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    view.clearAnimation();
                    _yDelta = Y;
                    time = System.currentTimeMillis();
                    break;
                case MotionEvent.ACTION_UP:
                    RelativeLayout.LayoutParams layoutParamsx = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    if (layoutParamsx.bottomMargin >= _xDelta - (_xDelta / 4) && !wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp - ((tmp + _xDelta / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.bottomMargin > _xDelta / 20) {
                                    params.bottomMargin = _xDelta / 20;
                                    wys = true;
                                }
                            }
                        };
                        if (delatmargin == 0) {
                            a.setDuration(500);
                        } else {
                            if (((tmp + _xDelta / 20) / delatmargin) * deltatime < 1500 && ((tmp + _xDelta / 20) / delatmargin) * deltatime >= 1) {
                                a.setDuration(((tmp + _xDelta / 20) / delatmargin) * deltatime);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        view.startAnimation(a);
                        wys = true;
                    } else if (layoutParamsx.bottomMargin <= _xDelta / 4 && wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp + ((_xDelta - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        if (delatmargin == 0) {
                            a.setDuration(500);
                        } else {
                            if (((_xDelta - tmp) / delatmargin) * deltatime * -1 < 1500 && ((_xDelta - tmp) / delatmargin) * deltatime * -1 >= 1) {
                                a.setDuration(((_xDelta - tmp) / delatmargin) * deltatime * -1);
                            } else {
                                a.setDuration(1499);
                            }
                        }
                        if (exlvGP != -1) {
                            expandableListView.collapseGroup(exlvGP);
                        }
                        view.startAnimation(a);
                        wys = false;
                    } else if (layoutParamsx.bottomMargin < _xDelta - (_xDelta / 4) && !wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp + ((_xDelta - tmp) * interpolatedTime));
                                view.setLayoutParams(params);
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    } else if (layoutParamsx.bottomMargin > _xDelta / 4 && wys) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        final int tmp = params.bottomMargin;
                        Animation a = new Animation() {

                            @Override
                            protected void applyTransformation(float interpolatedTime, Transformation t) {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                params.bottomMargin = (int) (tmp - ((tmp + _xDelta / 20) * interpolatedTime));
                                view.setLayoutParams(params);
                                if (params.bottomMargin > _xDelta / 20) {
                                    params.bottomMargin = _xDelta / 20;
                                }
                            }
                        };
                        a.setDuration(250);
                        view.startAnimation(a);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    _yDelta = Y - _yDelta;
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.bottomMargin -= _yDelta;
                    if (layoutParams.bottomMargin < _xDelta) {
                        layoutParams.bottomMargin = _xDelta;
                        wys = false;
                    } else if (layoutParams.bottomMargin > _xDelta / 20) {
                        layoutParams.bottomMargin = _xDelta / 20;
                        wys = true;
                    }
                    view.setLayoutParams(layoutParams);
                    delatmargin = _yDelta;
                    _yDelta = Y;
                    deltatime = (int) (System.currentTimeMillis() - time);
                    time = System.currentTimeMillis();
                    break;
            }
        }else if(view == findViewById(R.id.llm)&&nope.trigerek) {
                final int Y = (int) event.getRawX();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        view.clearAnimation();
                        _yDelta2 = Y;
                        time2 = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_UP:
                        RelativeLayout.LayoutParams layoutParamsx = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        if (layoutParamsx.rightMargin >= _xDelta2 - (_xDelta2 / 4) && !wys2) {
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            final int tmp = params.rightMargin;
                            Animation a = new Animation() {

                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                    params.rightMargin = (int) (tmp - ((tmp + _xDelta2 / 20) * interpolatedTime));
                                    view.setLayoutParams(params);
                                    if (params.rightMargin > _xDelta2 / 20) {
                                        params.rightMargin = _xDelta2 / 20;
                                        wys2 = true;
                                    }
                                }
                            };
                            if (delatmargin2 == 0) {
                                a.setDuration(500);
                            } else {
                                if (((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2 < 1500 && ((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2 >= 1) {
                                    a.setDuration(((tmp + _xDelta2 / 20) / delatmargin2) * deltatime2);
                                } else {
                                    a.setDuration(1499);
                                }
                            }
                            view.startAnimation(a);
                            wys2 = true;
                        } else if (layoutParamsx.rightMargin <= _xDelta2 / 4 && wys2) {
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            final int tmp = params.rightMargin;
                            Animation a = new Animation() {

                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                    params.rightMargin = (int) (tmp + ((_xDelta2 - tmp) * interpolatedTime));
                                    view.setLayoutParams(params);
                                }
                            };
                            if (delatmargin2 == 0) {
                                a.setDuration(500);
                            } else {
                                if (((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1 < 1500 && ((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1 >= 1) {
                                    a.setDuration(((_xDelta2 - tmp) / delatmargin2) * deltatime2 * -1);
                                } else {
                                    a.setDuration(1499);
                                }
                            }
                            view.startAnimation(a);
                            wys2 = false;
                        } else if (layoutParamsx.rightMargin < _xDelta2 - (_xDelta2 / 4) && !wys2) {
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            final int tmp = params.rightMargin;
                            Animation a = new Animation() {

                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                    params.rightMargin = (int) (tmp + ((_xDelta2 - tmp) * interpolatedTime));
                                    view.setLayoutParams(params);
                                }
                            };
                            a.setDuration(250);
                            view.startAnimation(a);
                        } else if (layoutParamsx.rightMargin > _xDelta2 / 4 && wys2) {
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            final int tmp = params.rightMargin;
                            Animation a = new Animation() {

                                @Override
                                protected void applyTransformation(float interpolatedTime, Transformation t) {
                                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                                    params.rightMargin = (int) (tmp - ((tmp + _xDelta2 / 20) * interpolatedTime));
                                    view.setLayoutParams(params);
                                    if (params.rightMargin > _xDelta2 / 20) {
                                        params.rightMargin = _xDelta2 / 20;
                                    }
                                }
                            };
                            a.setDuration(250);
                            view.startAnimation(a);
                        }
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        _yDelta2 = Y - _yDelta2;
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        layoutParams.rightMargin -= _yDelta2;
                        if (layoutParams.rightMargin < _xDelta2) {
                            layoutParams.rightMargin = _xDelta2;
                            wys2 = false;
                        } else if (layoutParams.rightMargin > _xDelta2 / 20) {
                            layoutParams.rightMargin = _xDelta2 / 20;
                            wys2 = true;
                        }
                        view.setLayoutParams(layoutParams);
                        delatmargin2 = _yDelta2;
                        _yDelta2 = Y;
                        deltatime2 = (int) (System.currentTimeMillis() - time2);
                        time2 = System.currentTimeMillis();
                        break;
                }
            }
        _root.invalidate();
        return true;
    }
    public void wyslij (View v) {
        if (wys) {
            if (!edti.getText().toString().equals("") && !edmes.getText().toString().equals("")) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                String strDate = format.format(now);
                CheckLogin checkLogin = new CheckLogin(false, mainactAL.this, findViewById(R.id.progressBar2),6564,  Integer.valueOf(nope.idk).toString() ,  Integer.valueOf(nope.ids).toString() , edmes.getText().toString(), edti.getText().toString(),strDate, resultp ->nope.refresh(pb2));
                checkLogin.execute("");
                edti.setText("");
                nope.tem = "";
                edmes.setText("");
                nope.tes = "";
                RelativeLayout lltmp = findViewById(R.id.llnm);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lltmp.getLayoutParams();
                final int tmp = params.bottomMargin;
                Animation a = new Animation() {

                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)lltmp.getLayoutParams();
                        params.bottomMargin = (int)(tmp + ((_xDelta-tmp) * interpolatedTime));
                        lltmp.setLayoutParams(params);
                    }
                };
                a.setDuration(250);
                lltmp.startAnimation(a);
            }
        }
    }
    public void specs(View v){
        if(findViewById(R.id.spec).getVisibility() == View.VISIBLE){
            findViewById(R.id.spec).setVisibility(View.GONE);
        }else {
            findViewById(R.id.spec).setVisibility(View.VISIBLE);
        }
    }
    public void puff(View v){
        v.setVisibility(View.GONE);
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Czy chcesz wyjść z aplikacji?").setPositiveButton("Tak", dialogClickListener)
                .setNegativeButton("Nie", dialogClickListener).show();
    }
    DialogInterface.OnClickListener dialogClickListener = (dialog, which)-> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    int pid = android.os.Process.myPid();
                    android.os.Process.killProcess(pid);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
    };

}